<?php
require_once __DIR__ . "/view/functions.php";

$trunkPageValues = [
    "vlans" => "",
    "code" => "",
    "ports" => "",
    "mode" => "create",
];

$command = request('command', null);
switch($command){
case 'trunk':
    $trunkPageValues['vlans'] = request('vlans');
    $trunkPageValues['ports'] = request('ports');
    $trunkPageValues['mode'] = request('mode');
    $mode = request('mode') == 'remove' ? true : false;

    foreach(explode("\n", $trunkPageValues['vlans']) as $vlan){
        $vlan = trim($vlan);
        if($vlan == "")
            continue;
        $str = "vlan $vlan \n";
        if($mode)
            $str .= 'no ';
        $str .= "tagged $trunkPageValues[ports]\n";

        $trunkPageValues['code'] .= $str;
    }


    viewPage('trunk', $trunkPageValues);
    break;
case null:
    viewPage('trunk', $trunkPageValues);
    break;
default:
    rawPage("'$command': command Not found");
}


function view($view, $data){
    $viewFile = __DIR__."/view/$view.php";
    if(!file_exists($viewFile))
        throw new RuntimeError("View not found");

    extract($data);
    ob_start();
    include "$viewFile";
    return ob_get_clean();
}

function rawPage($body){
    echo view('page', ['body' => $body]);
}

function viewPage($view, $data){
    rawPage(view($view,$data));
}
/**
 * gets a variable from $_REQUEST
 * 
 * @param string $request
 * @param string $default
 * @return string
 * @throws RuntimeException
 */
function request($request, $default = null) {
    if (isset($_REQUEST[$request]))
        return $_REQUEST[$request];

    if (func_num_args() < 2) {
        throw new RuntimeException("parameter $request not given and there is no default value for it");
    }

    return $default;
}

