<?php

$vlans = ["2","3","4","5","6","7","8","9","10","60","65","101","102","103","104","105","106","107","108","109","110","200","4000","4001","4002"];

for($i = 4003; $i < 4100; $i++)
    $vlans[] = $i;

$ports = '21-28';


foreach($vlans as $vlan){

    echo template_ports($vlan, $ports);

}

function template_ports($vlan, $ports){
    return <<<EOT
vlan $vlan
tagged $ports

EOT;
}
